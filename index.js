const http = require('http');   // Importa la librería HTTP de Node.js

const hostname = '0.0.0.0';   // Define el hostname como 0.0.0.0, que significa que la aplicación estará disponible desde cualquier dirección IP
const port = 3000;   // Define el puerto en el que se ejecutará el servidor en 3000

// Crea un servidor HTTP y define una función de devolución de llamada que se ejecutará cuando se reciba una solicitud
const server = http.createServer((req, res) => {
  res.statusCode = 200;    // Establece el código de estado HTTP en 200 (OK)
  res.setHeader('Content-Type', 'text/plain');   // Establece el tipo de contenido en text/plain
  res.end('Hola mundo soy Matias Galdames \n');   // Finaliza la respuesta con un mensaje
});

//# Inicia el servidor y define una función de devolución de llamada que se ejecutará cuando el servidor esté listo para recibir solicitudes
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);    // Imprime un mensaje en la consola indicando que el servidor está corriendo
});
