# Especificamos la imagen base para el contenedor
FROM node:14-alpine

# Definimos el directorio de trabajo para nuestra aplicación
WORKDIR /app

# Opcional: copiar archivos de paquetes package.json y package-lock.json
#COPY package*.json ./

# Instalamos las dependencias de nuestra aplicación
RUN npm install

# Copiamos los archivos de nuestra aplicación al directorio de trabajo
COPY ./index.js .

# Exponemos el puerto 3000 para permitir la conexión a nuestra aplicación
EXPOSE 3000

# Especificamos el comando para ejecutar nuestra aplicación al iniciar el contenedor
CMD [ "node", "index.js" ]
