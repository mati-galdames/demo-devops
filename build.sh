#!/bin/bash
echo 'Creating .env'  # Este script tiene como objetivo crear un archivo .env a partir de un archivo .env-template
IFS=$'\n'   # IFS se utiliza para establecer un delimitador de línea en el comando for
for line in $(cat .env-template)  # Este bucle for se ejecuta sobre cada línea en el archivo .env-template
do
	line="${line/=/}"  # Elimina el símbolo de igualdad de la línea y la asigna a la variable "line"
	branch="$(echo $CI_COMMIT_BRANCH | tr [a-z] [A-Z])"  # Convierte la rama de CI_COMMIT_BRANCH a mayúsculas y la asigna a la variable "branch"
	secret=$branch"-"$line   # Concatena la rama en mayúsculas con un guión y la línea para formar la variable secret
	value="$(google-cloud-sdk/bin/gcloud secrets versions access latest --secret=$secret --project=$GCLOUD_PROJECT_ID)"  # Obtiene el valor más reciente de la variable secret utilizando la CLI de Google Cloud SDK
	echo $line"="$value >> .env     # Agrega la línea a .env con la forma "line=value"
done
echo '.env finished'  # Informa al usuario de que se ha completado la creación del archivo .env