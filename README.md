# Creación de imagen en gcr.io y ejecución de deploy de esta misma al cluster de kubernetes creado anteriorente en GCP utilizando terraform como IaC.
### Descripción

Este proyecto se dividió en dos instancias: Build y Deploy. La instancia de Build se encargó de crear una imagen desde un Dockerfile y subirla al repositorio oficial de GCP (grc.io). Esta imagen contenía todas las dependencias necesarias para ejecutar el "Hola Mundo" en Node.js. La instancia de Deploy, por su parte, se encargó de desplegar la imagen creada en la instancia anterior utilizando el cluster creado con Terraform en GCP.

### Antes del Despliegue

Antes de realizar el despliegue se debe configurar 3 variables de entorno que nos ayudara con la comunicación con nuestro proyecto de GPC.
Las variables de entorno son las siguientes:

```GCLOUD_CLUSTER_NAME= demo-cluster-production```   --> Esta variable se extrae del nombre del cluster creado en GCP.

```GCLOUD_PROJECT_ID= my-firt-project-347213```   -->  Esta variable se obtiene del ID de nuestro proyecto creado en GCP.

```GCLOUD_SERVICE_KEY= contenido de archivo .json```   -->   Este es un estracto de la clave creada en GCP. Se debe crear en ```service-account``` en nuestro IAM del proyecto de GCP, aquí debemos generar una secret-key .json la cual ocuparemos como variable en nuestros proyectos.

### Proceso de Despliegue

Para realizar el despliegue, es necesario seguir los siguientes pasos:
* Ingresar a nuestro proyecto de Gitlab a la ruta CI/CD > Pipeline -> https://gitlab.com/mati-galdames/demo-devops/-/pipelines
* Ejecutar el pipeline, esto realizará un proceso de Build y Deploy. Estos procesos son manuales, esto da la seguridad de que en producción no pueda pasar algún error sin antes revisar bien la imagen que se debe crear y luego deployar.







